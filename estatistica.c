#include "estatistica.h"

double media(int n, double *v){

    double soma = 0;

    for (int i = 0; i < n; i++){
        soma += v[i];
    }

    soma /= n;
    return soma;
}