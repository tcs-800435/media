all: media

media: media.o estatistica.o desvio-padrao.o unity.o
	gcc -DUNITY_INCLUDE_DOUBLE media.o estatistica.o desvio-padrao.o unity.o -o media -lm

media.o: media.c estatistica.h desvio-padrao.h unity.h unity_internals.h
	gcc -DUNITY_INCLUDE_DOUBLE -c -o media.o media.c -lm

estatistica.o: estatistica.c estatistica.h
	gcc -DUNITY_INCLUDE_DOUBLE -c -o estatistica.o estatistica.c -lm

desvio-padrao.o: desvio-padrao.c estatistica.h desvio-padrao.h
	gcc -DUNITY_INCLUDE_DOUBLE -c -o desvio-padrao.o desvio-padrao.c -lm

unity.o: unity.c unity.h unity_internals.h
	gcc -DUNITY_INCLUDE_DOUBLE -c unity.c

clean:
	rm media *.o


