#include <stdio.h>
#include <stdlib.h>
#include "estatistica.h"
#include "desvio-padrao.h"
#include "unity.h"
#include "unity_internals.h"

void setUp(){}
void tearDown(){}

void test_media_valores_positivos() {
    double v[] = {1.0, 2.0, 3.0};
    double resultado = media(3, v);
    TEST_ASSERT_EQUAL_FLOAT(2.0, resultado);
}

void test_media_valores_negativos() {
    double v[] = {-1.0, -2.0, -3.0};
    double resultado = media(3, v);
    TEST_ASSERT_EQUAL_FLOAT(-2.0, resultado);
}

void test_desvio_valores_positivos() {
    double v[] = {1.0, 2.0, 3.0};
    double resultado = desvio(3, v);
    TEST_ASSERT_EQUAL_FLOAT(1, resultado);
}

void test_desvio_valores_negativos() {
    double v[] = {-1.0, -2.0, -3.0};
    double resultado = desvio(3, v);
    TEST_ASSERT_EQUAL_FLOAT(1, resultado);
}

void test_desvio_misturado() {
    double v[] = {-1.0, 2.0, -3.0};
    double resultado = desvio(3, v);
    TEST_ASSERT_EQUAL_FLOAT(2.51661158, resultado);
}

int main() {
    UNITY_BEGIN();
    RUN_TEST(test_media_valores_positivos);
    RUN_TEST(test_media_valores_negativos);
    RUN_TEST(test_desvio_valores_positivos);
    RUN_TEST(test_desvio_valores_negativos);
    RUN_TEST(test_desvio_misturado);
    return UNITY_END();;
}