#include <math.h>
#include "desvio-padrao.h"
#include "estatistica.h"

double desvio(int n, double *v){

    double x = 0,y = 0, desviop = 0;

    for (int a = 0; a < n; a++){
        y = v[a] - media(n,v);
        x += pow(y,2);
    }
    double z = 0;
    z = x/(n-1);
    desviop = sqrt(z);
}